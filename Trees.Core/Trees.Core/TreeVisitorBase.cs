﻿using System;

namespace Techniqly.Trees.Core
{
    public abstract class TreeVisitorBase<TNodeValue> where TNodeValue : IComparable<TNodeValue>
    {
        protected TreeVisitorBase(ITree<TNodeValue> tree)
        {
            Tree = tree;
        }

        protected ITree<TNodeValue> Tree { get; }

        public void Visit(Action<ITreeNode<TNodeValue>> onVisitAction = null)
        {
            DoVisit(Tree.Root, onVisitAction);
        }

        protected abstract void DoVisit(ITreeNode<TNodeValue> currentNode, Action<ITreeNode<TNodeValue>> onVisitAction);
    }
}