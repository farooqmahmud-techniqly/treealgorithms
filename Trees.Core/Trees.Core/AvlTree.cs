﻿using System;
using System.CodeDom;
using System.Diagnostics;

namespace Techniqly.Trees.Core
{
    public sealed class AvlTree<TNodeValue> : ITree<TNodeValue> where TNodeValue : IComparable<TNodeValue>
    {
        private readonly bool _autoBalance;
        private AvlTreeNode<TNodeValue> _root;

        public AvlTree(TNodeValue rootNodeValue) : this(rootNodeValue, false)
        {
            
        }

        public AvlTree(TNodeValue rootNodeValue, bool autoBalance)
        {
            _autoBalance = autoBalance;
            _root = new AvlTreeNode<TNodeValue>(rootNodeValue);
        }

        public AvlTreeNode<TNodeValue> Root
        {
            get { return _root; }
            set { _root = value; }
        }

        public int Height => Root.Height;

        public Action<AvlTreeNode<TNodeValue>> OnValueExists { get; set; }

        public AvlTreeNode<TNodeValue> Search(TNodeValue value)
        {
            return SearchInternal(value, Root);
        }
        
        public void Insert(TNodeValue value)
        {
            Debug.Assert(_root != null);

            var currentNode = _root;
            AvlTreeNode<TNodeValue> previousNode = null;

            while (currentNode != null)
            {
                if (value.CompareTo(currentNode.Value) == 0)
                {
                    OnValueExists?.Invoke(currentNode);
                    return;
                }

                previousNode = currentNode;

                if (value.CompareTo(currentNode.Value) < 0)
                {
                    currentNode = currentNode.Left;
                }
                else
                { 
                    currentNode = currentNode.Right;
                }
            }

            var newNode = new AvlTreeNode<TNodeValue>(value);
            Debug.Assert(previousNode != null);

            if (value.CompareTo(previousNode.Value) < 0)
            {
                previousNode.Left = newNode;
            }
            else
            {
                previousNode.Right = newNode;
            }

            Debug.WriteLine($"Inserted [{newNode.Value}]");

            if (_autoBalance)
            {
                Balance();
            }
        }

        private void Balance()
        {
            Debug.WriteLine($"Balancing started at root [{Root.Value}]");

            AvlTreeNode<TNodeValue> newRoot = BalanceNode(Root);

            if (newRoot != null && newRoot != Root)
            {
                Root = newRoot;
            }

            Debug.WriteLine($"Balancing finished. Root [{Root.Value}]");

        }

        private AvlTreeNode<TNodeValue> BalanceNode(AvlTreeNode<TNodeValue> node)
        {
            Debug.WriteLine($"Balance check [{node.Value}]");

            AvlTreeNode<TNodeValue> newRoot = null;

            if (node.Left != null)
            {
                node.Left = BalanceNode(node.Left);
            }

            if (node.Right != null)
            {
                node.Right = BalanceNode(node.Right);
            }

            var balanceFactor = node.BalanceFactor;
            Debug.WriteLine($"Balance factor {balanceFactor} [{node.Value}]");

            if (balanceFactor < -1)
            {
                //left heavy
                if (node.Left?.BalanceFactor <= -1)
                {
                    newRoot = LLRotate(node);
                }
                else
                {
                    newRoot = LRRotate(node);
                }
            }
            else if (balanceFactor > 1)
            {
                if (node.Right?.BalanceFactor >= 1)
                {
                    newRoot = RRRotate(node);
                }
                else
                {
                    newRoot = RLRotate(node);
                }
            }
            else
            {
                //still balanced
                newRoot = node;
            }

            return newRoot;
        }

        private AvlTreeNode<TNodeValue> RRRotate(AvlTreeNode<TNodeValue> node)
        {
            Debug.WriteLine($"RRRotate [{node.Value}]");

            var a = node;
            var b = a.Right;

            a.Right = b.Left;
            b.Left = a;

            return b;
        }

        private AvlTreeNode<TNodeValue> RLRotate(AvlTreeNode<TNodeValue> node)
        {
            Debug.WriteLine($"RLRotate [{node.Value}]");

            var a = node;
            var b = a.Right;
            var c = b.Left;

            a.Right = c.Left;
            b.Left = c.Right;
            c.Right = b;
            c.Left = a;

            return c;
        }

        private AvlTreeNode<TNodeValue> LLRotate(AvlTreeNode<TNodeValue> node)
        {
            Debug.WriteLine($"LLRotate [{node.Value}]");

            var a = node;
            var b = a.Left;

            a.Left = b.Right;
            b.Right = a;

            return b;
        }

        private AvlTreeNode<TNodeValue> LRRotate(AvlTreeNode<TNodeValue> node)
        {
            Debug.WriteLine($"LRRotate [{node.Value}]");

            var a = node;
            var b = a.Left;
            var c = b.Right;

            a.Left = c.Right;
            b.Right = c.Left;
            c.Left = b;
            c.Right = a;

            return c;
        }

        private static AvlTreeNode<TNodeValue> SearchInternal(TNodeValue value, AvlTreeNode<TNodeValue> currentNode)
        {
            if (currentNode == null)
            {
                return null;
            }

            if (value.CompareTo(currentNode.Value) == 0)
            {
                return currentNode;
            }

            return SearchInternal(
                value,
                value.CompareTo(currentNode.Value) < 0 ? currentNode.Left : currentNode.Right);
        }

    }
}