﻿namespace Techniqly.Trees.Core
{
    public interface ITreeNode<TValue>
    {
        AvlTreeNode<TValue> Left { get; }
        AvlTreeNode<TValue> Right { get; }
        TValue Value { get; }
    }
}