﻿using System;

namespace Techniqly.Trees.Core
{
    public sealed class AvlTreeNode<TValue> : ITreeNode<TValue>
    {
        public AvlTreeNode(TValue value)
        {
            Value = value;
            Left = null;
            Right = null;
        }

        public TValue Value { get; }
        public AvlTreeNode<TValue> Left { get; internal set; }
        public AvlTreeNode<TValue> Right { get; internal set; }

        public int Height => ComputeHeight(this);

        public int BalanceFactor => ComputeBalanceFactor(this);

        private int ComputeHeight(AvlTreeNode<TValue> node)
        {
            if (node == null)
            {
                return 0;
            }

            var leftSubtreeHeight = ComputeHeight(node.Left);
            var rightSubtreeHeight = ComputeHeight(node.Right);

            return Math.Max(leftSubtreeHeight, rightSubtreeHeight) + 1;
        }

        private int ComputeBalanceFactor(AvlTreeNode<TValue> node)
        {
            var leftSubtreeHeight = ComputeHeight(node.Left);
            var rightSubtreeHeight = ComputeHeight(node.Right);

            return -leftSubtreeHeight + rightSubtreeHeight;
        }
        
    }
}