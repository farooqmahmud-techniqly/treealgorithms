﻿using System;

namespace Techniqly.Trees.Core
{
    public interface ITree<TNodeValue> where TNodeValue : IComparable<TNodeValue>
    {
        AvlTreeNode<TNodeValue> Root { get; }
    }
}