﻿using System;

namespace Techniqly.Trees.Core
{
    public sealed class InorderTreeVisitor<TNodeValue> : TreeVisitorBase<TNodeValue> where TNodeValue : IComparable<TNodeValue>
    {
        public InorderTreeVisitor(ITree<TNodeValue> tree) : base(tree)
        {
            
        }
        
        protected override void DoVisit(ITreeNode<TNodeValue> currentNode, Action<ITreeNode<TNodeValue>> onVisitAction)
        {
            if (currentNode == null)
            {
                return;
            }

            DoVisit(currentNode.Left, onVisitAction);
            onVisitAction?.Invoke(currentNode);
            DoVisit(currentNode.Right, onVisitAction);
        }
    }
}