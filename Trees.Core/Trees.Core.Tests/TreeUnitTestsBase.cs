﻿namespace Techniqly.Trees.Core.Tests
{
    public abstract class TreeUnitTestsBase
    {
        protected AvlTree<int> Tree { get; set; }

        protected static AvlTree<int> CreateTree()
        {
            var tree = new AvlTree<int>(100);
            tree.Insert(50);
            tree.Insert(150);
            tree.Insert(25);
            tree.Insert(200);
            tree.Insert(250);
            tree.Insert(10);
            tree.Insert(12);
            tree.Insert(210);
            tree.Insert(70);
            tree.Insert(140);
            tree.Insert(190);
            tree.Insert(30);
            tree.Insert(215);

            return tree;
        }
    }
}