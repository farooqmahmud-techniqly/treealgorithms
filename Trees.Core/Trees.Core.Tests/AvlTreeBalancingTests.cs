﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;

namespace Techniqly.Trees.Core.Tests
{
    [TestFixture]
    public class AvlTreeBalancingTests
    {
        [Test]
        public void LLRotateTest()
        {
            var tree = new AvlTree<int>(50, true);
            tree.Insert(45);
            tree.Insert(41);

            tree.Root.Value.Should().Be(45);
            tree.Root.Left.Value.Should().Be(41);
            tree.Root.Right.Value.Should().Be(50);
        }

        [Test]
        public void RRRotateTest()
        {
            var tree = new AvlTree<int>(50, true);
            tree.Insert(41);
            tree.Insert(55);
            tree.Insert(60);
            tree.Insert(65);

            tree.Root.Value.Should().Be(50);
            tree.Root.Left.Value.Should().Be(41);
            tree.Root.Right.Value.Should().Be(60);
            tree.Root.Right.Left.Value.Should().Be(55);
            tree.Root.Right.Right.Value.Should().Be(65);
        }

        [TestCase]
        public void BalancingTests()
        {
            var random = new Random();
            var iterations = 100000;
            var tree = new AvlTree<int>(random.Next(int.MinValue, int.MaxValue), false);

            for (var i = 0; i < iterations; i++)
            {
                var x = random.Next(int.MinValue, int.MaxValue);
                tree.Insert(x);
            }

            Console.WriteLine($"Tree height: {tree.Height}");
            
            var visitor = new InorderTreeVisitor<int>(tree);

            var maxBalanceFactor = int.MinValue;
            var minBalanceFactor = int.MaxValue;

            visitor.Visit(node =>
            {
                var avlNode = node as AvlTreeNode<int>;
                var bf = avlNode.BalanceFactor;

                if (bf > maxBalanceFactor)
                {
                    maxBalanceFactor = bf;
                }

                if (bf < minBalanceFactor)
                {
                    minBalanceFactor = bf;
                }
            });

            Console.WriteLine($"BfMin: {minBalanceFactor}, BfMax: {maxBalanceFactor}");
        }
    }
}
