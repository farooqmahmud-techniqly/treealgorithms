﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace Techniqly.Trees.Core.Tests
{
    [TestFixture]
    public class InorderTreeVisitorUnitTests : TreeUnitTestsBase
    {
        [SetUp]
        public void SetUp()
        {
            Tree = CreateTree();
        }

        [Test]
        public void Visit_OutputsNodesInOrder()
        {
            var nodes = new List<int>();
            Action<ITreeNode<int>> onVisitAction = node => nodes.Add(node.Value);

            var visitor = new InorderTreeVisitor<int>(Tree);
            visitor.Visit(onVisitAction);

            var expectedSequence = new[] {10, 12, 25, 30, 50, 70, 100, 140, 150, 190, 200, 210, 215, 250};
            nodes.SequenceEqual(expectedSequence).Should().BeTrue();
        }
    }
}