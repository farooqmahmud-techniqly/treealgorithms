﻿using FluentAssertions;
using NUnit.Framework;

namespace Techniqly.Trees.Core.Tests
{
    [TestFixture]
    public class TreeNodeUnitTests
    {
        [Test]
        public void CalculateHeightOfNodes()
        {
            var root = new AvlTreeNode<int>(90);
            root.Left = new AvlTreeNode<int>(50);
            root.Left.Right = new AvlTreeNode<int>(60);
            root.Right = new AvlTreeNode<int>(99);
            root.Right.Left = new AvlTreeNode<int>(70);
            root.Right.Left.Right = new AvlTreeNode<int>(98);

            root.Height.Should().Be(4);
            root.Left.Height.Should().Be(2);
            root.Left.Right.Height.Should().Be(1);
            root.Right.Height.Should().Be(3);
            root.Right.Left.Height.Should().Be(2);
            root.Right.Left.Right.Height.Should().Be(1);
        }

        [Test]
        public void HeightOfNewTreeIsOne()
        {
            var node = new AvlTreeNode<int>(100);
            node.Height.Should().Be(1);
        }
    }
}