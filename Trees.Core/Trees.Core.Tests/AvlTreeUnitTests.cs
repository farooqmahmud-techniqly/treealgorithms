﻿using FluentAssertions;
using NUnit.Framework;

namespace Techniqly.Trees.Core.Tests
{
    [TestFixture]
    public class AvlTreeUnitTests : TreeUnitTestsBase
    {
        [SetUp]
        public void SetUp()
        {
            Tree = CreateTree();
        }
        
        [Test]
        public void Constructor_SetsRootNode()
        {
            var tree = new AvlTree<int>(100);
            tree.Root.Value.Should().Be(100);
        }

        [Test]
        public void Height_CalculatesHeight()
        {
            Tree.Height.Should().Be(6);
        }

        [Test]
        public void Insert_InsertsNodesIntoTree()
        {
            var root = Tree.Root;
            root.Left.Value.Should().Be(50);
            root.Left.Left.Value.Should().Be(25);
            root.Left.Left.Left.Value.Should().Be(10);
            root.Left.Right.Value.Should().Be(70);
            root.Left.Left.Left.Right.Value.Should().Be(12);
            root.Right.Value.Should().Be(150);
            root.Right.Right.Value.Should().Be(200);
            root.Right.Right.Right.Value.Should().Be(250);
            root.Right.Left.Value.Should().Be(140);
            root.Right.Right.Right.Left.Value.Should().Be(210);
            root.Right.Right.Right.Left.Right.Value.Should().Be(215);
            root.Right.Right.Left.Value.Should().Be(190);
            root.Left.Left.Right.Value.Should().Be(30);
        }

        [Test]
        public void Insert_WhenValueExists_AndOnValueExistsDelegateIsNotNull_ExecutesDelegate()
        {
            AvlTreeNode<int> existingNode = null;

            Tree.OnValueExists = node => { existingNode = node; };
            Tree.Insert(150);

            existingNode.Value.Should().Be(150);
        }

        [Test]
        public void Insert_WhenValueExists_AndOnValueExistsDelegateNull_DoesANoOp()
        {
            Tree.OnValueExists.Should().BeNull();
            Tree.Insert(150);
            Tree.Search(150).Should().NotBeNull();
        }

        [Test]
        public void Search_WhenValueDoesNotExist_ReturnsNull()
        {
            Tree.Search(211).Should().BeNull();
        }

        [Test]
        [TestCase(50)]
        [TestCase(25)]
        [TestCase(10)]
        [TestCase(70)]
        [TestCase(12)]
        [TestCase(150)]
        [TestCase(200)]
        [TestCase(250)]
        [TestCase(140)]
        [TestCase(210)]
        [TestCase(215)]
        [TestCase(190)]
        [TestCase(30)]
        public void Search_WhenValueExists_ReturnsNode(int value)
        {
            var node = Tree.Search(value);
            node.Should().NotBeNull();
            node.Value.Should().Be(value);
        }

        [TestCase(100, 1)]
        [TestCase(50, -2)]
        [TestCase(25, -1)]
        [TestCase(10, 1)]
        [TestCase(12, 0)]
        [TestCase(30, 0)]
        [TestCase(70, 0)]
        [TestCase(150, 3)]
        [TestCase(140, 0)]
        [TestCase(190, 0)]
        [TestCase(215, 0)]
        [TestCase(200, 2)]
        [TestCase(250, -2)]
        [TestCase(210, 1)]
        public void BalanceFactor_ComputesBalanceFactor(int nodeValue, int expected)
        {
            var node = Tree.Search(nodeValue);
            node.BalanceFactor.Should().Be(expected);
        }
    }
}